/* 

 * Copyright 2017 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A helper class to load properties (from a *.properties file) into an
 * arbitrary object. Properties may be of any type (primitives/reference types).
 * <p>
 * Consider the following example:
 * <pre>
 *dao.defaultSchema = public
 *   dao.sequenceFactoryName = com.iamcodepoet.sandbox.DefaultSequenceFactory
 *
 *   dao.connectionFactory = class com.iamcodepoet.sandbox.db.SqLiteConnectionFactory
 *   dao.connectionFactory.databaseName = people.db
 *   dao.connectionFactory.autoCommit = true
 * </pre>
 * <p>
 * The variable <b>dao</b> is expected to have three properties: defaultSchema,
 * sequenceFactoryName, and connectionFactory. To set a property value, the
 * variable (dao in this case) is check for a setter. If the object's class has
 * multiple setters, the first with a parameter type to which the input can be
 * cast will be used.
 * </p>
 * <p>
 * As a last resort, of no setter is found, it will attempt to find a method
 * with the following signature: add&lt;Inferred Value Class Simple
 * Name&gt;(&lt;Inferred Type&gt;);.
 * </p>
 * <p>
 * A class instance can be assigned by setting a property value with the prefix
 * <b>class</b> followed by a space and the fully qualified class name. For
 * example: <i>dao.connectionFactory = class
 * com.iamcodepoet.sandbox.db.SqLiteConnectionFactory</i>. The setter for this
 * property must have a parameter type that is assignable from the class name in
 * the property's value.
 * </p>
 * <p>
 * A Properties file can be loaded as follows:
 * </p>
 * <pre>
 * DaoFactory factory= DaoFactory.getInstance();
 * PropertyLoader loader = new PropertyLoader(factory, "dao");
 * loader.load(props);
 * </pre>
 * <p>
 * In this case, <b>factory</b> is the object, whose attributes will be loaded
 * from the <b>dao</b> root property in the properties file.
 * </p>
 *
 * @author Roberto C. Benitez
 */
public class PropertyLoader
{

    private final static String VARIABLE_REGEX = "^\\$\\{[a-zA-z_]\\w*(\\.[a-zA-z_]\\w*)*\\}$";
    private final static String IDENT_NAME_REGEX = "[a-zA-z_]\\w*";
    private final static String INT_REGEX = "^[0-9]+$";
    private final static String LONG_REGEX = "^[0-9]+[lL]?$";
    private final static String DOUBLE_REGEX = "^[0-9]+(\\.\\d+)?[dD]?$";
    private final static String FLOAT_REGEX = "^[0-9]+(\\.\\d+)?[fF]?$";
    private final static String BOOLEAN_REGEX = "^(true|True|TRUE|false|False|FALSE)$";
    private final static String ISO_DATE_REGEX = "^\\d{4}\\-\\d{2}\\-\\d{2}$";

    private final Object root;
    private final String rootNode;

    private OrderedProperties properties;
    private Set<String> keys;

    private final Map<String, List<Method>> setterMethods = new HashMap<>();
    private final Map<String, List<Method>> adderMethods = new HashMap<>();

    private final Map<String, Object> variables = new HashMap<>();

    public PropertyLoader(Object root, String rootNode)
    {
        this.root = root;
        this.rootNode = rootNode;
    }

    public PropertyLoader(Object root)
    {
        this.root = root;
        this.rootNode = "root";
    }

    public void load(File propertiesFile) throws IOException
    {
        OrderedProperties props = new OrderedProperties();

        try (InputStream is = new FileInputStream(propertiesFile))
        {
            props.load(is);
        }

        load(props);
    }

    public void load(OrderedProperties props)
    {
        this.properties = props;
        this.keys = props.stringPropertyNames();

        load(root, rootNode);
    }

    private void load(Object object, String nodeName)
    {
        List<String> objectKeys = getObjectKeys(nodeName);

        if (objectKeys.isEmpty())
        {
            return;
        }

        for (String key : objectKeys)
        {
            String value = properties.getProperty(key);
            if (value == null)
            {
                continue;
            }

            int pos = key.lastIndexOf('.');
            String propertyName = (pos <= 0) ? key : key.substring(pos + 1);

            Object property;

            if (value.matches(VARIABLE_REGEX))
            {
                property = loadPropertyFromVariables(value, object, propertyName, key);
            }
            else
            {
                if (isQualifiedClassName(value))
                {
                    property = loadClassInstanceToProperty(object, propertyName, key, value);
                }
                else
                {
                    property = loadValueIntoProperty(object, propertyName, value, key);
                }

                variables.put(key, property);
            }

            if (property == null)
            {
                continue;
            }

            load(property, key);
        }
    }

    private Object loadPropertyFromVariables(String variableName, Object object, String propertyName, String propertyNodeName)
    {
        String var = variableName.substring(2, variableName.length() - 1);
        Object property = variables.get(var);
        if (property == null)
        {
            throw new RuntimeException("Variable not found. " + variableName);
        }

        String className = object.getClass().getName();
        if (!setterMethods.containsKey(className))
        {
            loadSettersAndAdders(object.getClass());
        }
        Optional<Method> setter = setterMethods.get(className)
                .stream().filter(setterMethodFilter(propertyName, property.getClass()))
                .findFirst();
        if (setter.isPresent())
        {
            invokeMethod(setter.get(), object, property, propertyNodeName, propertyName);
        }
        else
        {
            Optional<Method> adder = adderMethods.get(className).stream().filter(adderMethodFilter(property.getClass()))
                    .findFirst();

            if (adder.isPresent())
            {
                invokeMethod(adder.get(), object, property, propertyNodeName, propertyName);
            }

            String msg = String.format("Unable to load value \"%s\" into %s", variableName, propertyNodeName);
            throw new RuntimeException(msg);
        }
        return property;
    }

    /**
     * Try to find a suitable method to load the value. Methods with name
     * pattern <b>set&lt;propertyName&gt;</b>--if the input value can be cast to
     * match the method parameter type. If no such method is found, methods with
     * pattern <b>add&lt;value inferred type&gt; (Simple Class Name)</b> and the
     * parameter that is assignable from the input value's inferred type will be
     * searched. If neither approach succeeds, a {@link RuntimeException} will
     * be thrown.
     *
     * @param object object class into which the property will be loaded
     * @param objectClass object class into which the property will be loaded
     * @param propertyName object property into which the value--cast to the
     * appropriate type--will be loaded
     * @param value original input value
     * @param variableName the full variable name--for exception message, if an
     * exception needs to be thrown
     * @return value cast to an appropriate type.
     */
    private Object loadValueIntoProperty(Object object, String propertyName, String value, String variableName)
    {
        Class objectClass = object.getClass();
        String className = objectClass.getName();

        if (!setterMethods.containsKey(className))
        {
            loadSettersAndAdders(objectClass);
        }

        Object property;

        final String setterName = "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);

        List<Method> setters = setterMethods.get(className)
                .stream().filter(e -> e.getName().equals(setterName))
                .collect(Collectors.toList());
        if (setters.isEmpty())
        {
            property = castProperty(value);
            Optional<Method> adder = adderMethods.get(className).stream().filter(adderMethodFilter(property.getClass()))
                    .findFirst();

            if (adder.isPresent())
            {
                invokeMethod(adder.get(), object, property, variableName, propertyName);
            }

            String msg = String.format("Unable to load value \"%s\" into %s", value, variableName);
            throw new RuntimeException(msg);
        }
        else
        {
            Pair<Method, Object> pair = findMutatorForInputValue(setters, value, variableName, propertyName);
            property = pair.getRight();
            invokeMethod(pair.getLeft(), object, property, variableName, propertyName);
        }
        return property;
    }

    private Pair<Method, Object> findMutatorForInputValue(List<Method> availableMethods, String inputValue, String variableName, String propertyName)
            throws NumberFormatException
    {
        Method method = null;
        Object property = null;
        for (int i = 0; i < availableMethods.size(); i++)
        {
            Method temp = availableMethods.get(i);
            Class argType = temp.getParameterTypes()[0];
            Optional opt = tryToCastValue(argType, inputValue);

            if (opt.isPresent())
            {
                property = opt.get();
                method = temp;
                break;
            }
        }

        if (method == null)
        {
            String msg = String.format("Unable to set value \"%s\" to %s", inputValue, variableName);
            throw new RuntimeException(msg);
        }

        return new Pair<>(method, property);
    }

    private Optional<Object> tryToCastValue(Class argType, String value) throws NumberFormatException
    {
        Object casted;
        if (argType == String.class)
        {
            casted = value;
        }
        else if (isInteger(argType) && isInteger(value))
        {
            casted = Integer.parseInt(value.replaceAll("[^\\d.]", ""));
        }
        else if (isLong(argType) && isLong(value))
        {
            casted = Long.parseLong(value.replaceAll("[^\\d.]", ""));
        }
        else if (isBoolean(argType) && isBoolean(value))
        {
            casted = Boolean.parseBoolean(value);
        }
        else if (isDouble(argType) && isDouble(value))
        {
            casted = Double.parseDouble(value.replaceAll("[^\\d.]", ""));
        }
        else if (isFloat(argType) && isFloat(value))
        {
            casted = Float.parseFloat(value.replaceAll("[^\\d.]", ""));
        }
        else if (isChar(argType) && value.length() == 1)
        {
            casted = value.isEmpty() ? '\0' : value.charAt(0);
        }
        else if (argType == java.util.Date.class && isIsoDate(value))
        {
            LocalDate ld = LocalDate.parse(value);
            casted = java.util.Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        else if (argType == java.sql.Date.class && isIsoDate(value))
        {
            LocalDate ld = LocalDate.parse(value);
            casted = java.sql.Date.valueOf(ld);
        }
        else
        {
            casted = null;
        }

        return Optional.ofNullable(casted);
    }

    private static boolean isChar(Class argType)
    {
        return argType == char.class || argType == Character.class;
    }

    private static boolean isFloat(Class argType)
    {
        return argType == Float.class || argType == Float.class;
    }

    private static boolean isDouble(Class argType)
    {
        return argType == double.class || argType == Double.class;
    }

    private static boolean isBoolean(Class argType)
    {
        return argType == boolean.class || argType == Boolean.class;
    }

    private static boolean isLong(Class argType)
    {
        return argType == long.class || argType == Long.class;
    }

    private static boolean isInteger(Class argType)
    {
        return argType == int.class || argType == Integer.class;
    }

    private Object loadClassInstanceToProperty(Object object, String propertyName, String variableName, String value) throws RuntimeException
    {
        Class propertyClass = loadClass(value);
        Object property = createClassInstance(propertyClass);
        Method method = getMethodByPropertyType(propertyName, object.getClass(), property.getClass());
        invokeMethod(method, object, property, variableName, propertyName);

        return property;
    }

    private Object castProperty(String value) throws NumberFormatException
    {
        Object property;
        if (value == null || value.trim().isEmpty())
        {
            property = null;
        }
        else if (isQualifiedClassName(value))
        {
            Class propertyClass = loadClass(value);
            property = createClassInstance(propertyClass);
        }
        else if (isInteger(value))
        {
            property = Integer.parseInt(value);
        }
        else if (isDouble(value))
        {
            property = Double.parseDouble(value);
        }
        else if (isBoolean(value))
        {
            property = Boolean.parseBoolean(value.toLowerCase());

        }
        else if (isFloat(value))
        {
            property = Float.parseFloat(value);
        }
        else if (isIsoDate(value))
        {
            LocalDate ld = LocalDate.parse(value);
            property = Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        else
        {
            property = value;
        }

        return property;
    }

    private static boolean isQualifiedClassName(String value)
    {
        return value.startsWith("class ");
    }

    private void invokeMethod(Method method, Object object, Object property, String variableName, String propertyName) throws RuntimeException
    {
        try
        {
            method.invoke(object, property);
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
        {
            String msg = String.format("Unable to set property %s.%s.  %s", variableName, propertyName, e.getMessage());
            throw new RuntimeException(msg, e);
        }
    }

    private Method getMethodByPropertyType(String propertyName, Class objectClass, Class propertyClass)
    {
        final String nameRoot = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);

        String objectClassName = objectClass.getName();

        if (!setterMethods.containsKey(objectClassName))
        {
            loadSettersAndAdders(objectClass);
        }

        //look for a set* method
        Optional<Method> opt = setterMethods.get(objectClassName).stream()
                .filter(setterMethodFilter(nameRoot, propertyClass))
                .findFirst();

        if (opt.isPresent())
        {
            return opt.get();
        }

        //look for an add* method
        opt = adderMethods.get(objectClassName).stream()
                .filter(adderMethodFilter(propertyClass))
                .findFirst();

        if (opt.isPresent())
        {
            return opt.get();
        }

        String msg = String.format("property %s.%s(%s) not found.", objectClassName, propertyName, propertyClass);
        throw new RuntimeException(msg);
    }

    private Predicate<Method> setterMethodFilter(String name, Class propertyType)
    {
        return method ->
        {
            Class argType = method.getParameterTypes()[0];
            String searchName = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
            return (method.getName().equals(searchName)
                    && (argType.isAssignableFrom(propertyType)
                    || (isBoxedPrimitive(propertyType) && isWrapperPrimitivePair(propertyType, argType))));
        };
    }

    private Predicate<Method> adderMethodFilter(Class propertyType)
    {
        return method ->
        {
            Class argType = method.getParameterTypes()[0];
            return (argType.isAssignableFrom(propertyType) && method.getName().equals("add" + propertyType.getSimpleName()));
        };
    }

    private List<String> getObjectKeys(String node)
    {
        String nodeRegEx = "^" + node + "\\." + IDENT_NAME_REGEX + "$";

        List<String> objectKeys = new LinkedList<>();
        keys.stream().filter(e -> e.matches(nodeRegEx))
                .forEach(e ->
                {
                    objectKeys.add(e);
                });
        return objectKeys;
    }

    /**
     * Load class separately simple to properly catch a a
     * {@link ClassNotFoundException}--such that a proper exception message is
     * generated
     *
     * @param value
     * @return
     */
    private Class loadClass(String value)
    {
        try
        {
            return Class.forName(value.substring(6));
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    private Object createClassInstance(Class zclass)
    {

        try
        {
            return zclass.newInstance();
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e.getMessage() + ". Make sure class has a public constructor", e);
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e.getMessage() + ". Make sure class has a no-argument constructor", e);
        }
    }

    private boolean isInteger(String string)
    {
        return string.matches(INT_REGEX);
    }

    private boolean isLong(String string)
    {
        return string.matches(LONG_REGEX);
    }

    private boolean isDouble(String string)
    {
        return string.matches(DOUBLE_REGEX);
    }

    private boolean isFloat(String string)
    {
        return string.matches(FLOAT_REGEX);
    }

    private boolean isBoolean(String string)
    {
        return string.matches(BOOLEAN_REGEX);
    }

    private boolean isIsoDate(String string)
    {
        return string.matches(ISO_DATE_REGEX);
    }

    private void loadSettersAndAdders(Class zclass)
    {
        List<Method> setterList = new ArrayList<>();
        List<Method> adderList = new ArrayList<>();
        for (Method method : zclass.getDeclaredMethods())
        {
            if (method.getParameters().length != 1)
            {
                continue;
            }
            String name = method.getName();
            if (name.startsWith("set"))
            {
                setterList.add(method);
            }
            else
            {
                if (name.startsWith("add"))
                {
                    adderList.add(method);
                }
            }
        }

        String name = zclass.getName();

        setterMethods.put(name, setterList);
        adderMethods.put(name, adderList);

    }

    private boolean isBoxedPrimitive(Class type)
    {
        return (type == Double.class || type == Float.class || type == Long.class
                || type == Integer.class || type == Short.class || type == Character.class
                || type == Byte.class || type == Boolean.class);

    }

    private boolean isWrapperPrimitivePair(Class type1, Class type2)
    {
        return ((type1 == Integer.class && type2 == int.class)
                || (type1 == Double.class && type2 == double.class)
                || (type1 == Boolean.class && type2 == boolean.class)
                || (type1 == Long.class && type2 == long.class)
                || (type1 == Float.class && type2 == float.class)
                || (type1 == Short.class && type2 == short.class)
                || (type1 == Byte.class && type2 == byte.class)
                || (type1 == Character.class && type2 == char.class));

    }

    private final class Pair<L, R>
    {

        private final L left;
        private final R right;

        public Pair(L left, R right)
        {
            this.left = left;
            this.right = right;
        }

        public L getLeft()
        {
            return left;
        }

        public R getRight()
        {
            return right;
        }

        @Override
        public int hashCode()
        {
            int hash = 3;
            hash = 89 * hash + Objects.hashCode(this.left);
            hash = 89 * hash + Objects.hashCode(this.right);
            return hash;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            final Pair<?, ?> other = (Pair<?, ?>) obj;
            if (!Objects.equals(this.left, other.left))
            {
                return false;
            }
            if (!Objects.equals(this.right, other.right))
            {
                return false;
            }
            return true;
        }

        @Override
        public String toString()
        {
            return "(" + left + "," + right + ")";
        }

    }
}
