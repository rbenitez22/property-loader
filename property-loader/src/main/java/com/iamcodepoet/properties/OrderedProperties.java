/*
 * Copyright 2017 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.properties;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 *
 * @author Roberto C. Benitez
 */
public class OrderedProperties extends Properties
{
    private final Map<Object,Object> properties = new LinkedHashMap<>();

    @Override
    public Set<String> stringPropertyNames()
    {
        Set<String> keys= new LinkedHashSet<>();
        properties.keySet().stream().forEach((key) ->
        {
            keys.add((String)key);
        });
        
        return keys;
    }

    @Override
    public Enumeration<?> propertyNames()
    {
        return keys();
    }

    @Override
    public synchronized Enumeration<Object> keys()
    {
        return Collections.enumeration(properties.keySet());
    }

    @Override
    public String getProperty(String key, String defaultValue)
    {
        String string= getProperty(key);
        return (string == null)? defaultValue : string;
    }

    @Override
    public String getProperty(String key)
    {
        Object object= properties.get(key);
        String string = (object instanceof String)? (String)object : null;
        return ((string == null) && (defaults != null)) ? defaults.getProperty(key) : string;
    }

    @Override
    public void save(OutputStream out, String comments)
    {
        try
        {
            store(out, comments);
        }
        catch (IOException ex)
        {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public synchronized Object setProperty(String key, String value)
    {
        return properties.put(key, value);
    }

    @Override
    public synchronized Object putIfAbsent(Object key, Object value)
    {
        return properties.putIfAbsent(key, value); 
    }

    @Override
    public synchronized void putAll(Map<? extends Object, ? extends Object> t)
    {
        properties.putAll(t); 
    }

    @Override
    public synchronized Object put(Object key, Object value)
    {
        return properties.put(key, value); 
    }

    @Override
    public synchronized Object merge(Object key, Object value, BiFunction<? super Object, ? super Object, ? extends Object> remappingFunction)
    {
        return properties.merge(key, value, remappingFunction); 
    }

    @Override
    public synchronized Object compute(Object key, BiFunction<? super Object, ? super Object, ? extends Object> remappingFunction)
    {
        return properties.compute(key, remappingFunction); 
    }

    @Override
    public synchronized Object computeIfPresent(Object key, BiFunction<? super Object, ? super Object, ? extends Object> remappingFunction)
    {
        return properties.computeIfPresent(key, remappingFunction); 
    }

    @Override
    public synchronized Object computeIfAbsent(Object key, Function<? super Object, ? extends Object> mappingFunction)
    {
        return properties.computeIfAbsent(key, mappingFunction); 
    }

    @Override
    public synchronized Object replace(Object key, Object value)
    {
        return properties.replace(key, value); 
    }

    @Override
    public synchronized boolean replace(Object key, Object oldValue, Object newValue)
    {
        return properties.replace(key, oldValue, newValue); 
    }

    @Override
    public synchronized boolean remove(Object key, Object value)
    {
        return properties.remove(key, value); 
    }

    @Override
    public synchronized void replaceAll(BiFunction<? super Object, ? super Object, ? extends Object> function)
    {
        properties.replaceAll(function); 
    }

    @Override
    public synchronized void forEach(BiConsumer<? super Object, ? super Object> action)
    {
        properties.forEach(action); 
    }

    @Override
    public synchronized Object getOrDefault(Object key, Object defaultValue)
    {
        return properties.getOrDefault(key, defaultValue); 
    }

    @Override
    public synchronized int hashCode()
    {
        return properties.hashCode(); 
    }

    @Override
    public synchronized boolean equals(Object object)
    {
        if(this == object){return true;}
        if(object instanceof OrderedProperties == false){return false;}
        
        OrderedProperties other=(OrderedProperties)object;
        if(size() != other.size()){return false;}
        
        return properties.equals(other.properties);
    }

    @Override
    public Collection<Object> values()
    {
        return properties.values(); 
    }

    @Override
    public Set<Entry<Object, Object>> entrySet()
    {
        return properties.entrySet(); 
    }

    @Override
    public Set<Object> keySet()
    {
        return properties.keySet(); 
    }

    @Override
    public synchronized String toString()
    {
        return properties.toString(); 
    }

    @Override
    public synchronized void clear()
    {
        properties.clear(); 
    }

    @Override
    public synchronized Object remove(Object key)
    {
        return properties.remove(key); 
    }

    @Override
    public synchronized Object get(Object key)
    {
        return properties.get(key); 
    }

    @Override
    public synchronized boolean containsKey(Object key)
    {
        return properties.containsKey(key); 
    }

    @Override
    public boolean containsValue(Object value)
    {
        return properties.containsValue(value); 
    }

    @Override
    public synchronized boolean contains(Object value)
    {
        return properties.containsValue(value);
    }

    @Override
    public synchronized Enumeration<Object> elements()
    {
        return Collections.enumeration(properties.values());
    }

    @Override
    public synchronized boolean isEmpty()
    {
        return properties.isEmpty(); 
    }

    @Override
    public synchronized int size()
    {
        return properties.size(); 
    }
    
}
