/*
 * Copyright 2017 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.sandbox;

import com.iamcodepoet.sandbox.db.ConnectionFactory;
import com.iamcodepoet.sandbox.db.SqLiteConnectionFactory;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Roberto C. Benitez
 */
public class DaoFactoryTest
{
    
    public DaoFactoryTest()
    {
    }
    
    @Test public void testDaoFactory()
    {
        ConnectionFactory factory = new SqLiteConnectionFactory("people.db");
        DaoFactory daoFactory = DaoFactory.getInstance();
        daoFactory.setConnectionFactory(factory);
        daoFactory.setDefaultSchema("public");
        
        try(PersonDao dao = DaoFactory.getInstance().createPersonDao())
        {
            dao.query().forEach(e->
            {
                System.out.printf("%s, %s\n",e.getLastName(),e.getFirstName());
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            assert 0 > 1;
        }
        
        assert 0 > 1;
    }
    
}
