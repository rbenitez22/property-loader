/*
 * Copyright 2017 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.sandbox;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Roberto C. Benitez
 */
public class PersonDao implements AutoCloseable
{
    private final static String TABLE_NAME="People";
    
    private final String schema;
    private final Connection connection;
    private final String identiferQuoteString;

    public PersonDao(Connection connection) throws SQLException
    {
        this(connection, null);
    }

    
    public PersonDao(Connection connection,String schema) throws SQLException
    {
        this.connection = connection;
        this.schema = schema;
        this.identiferQuoteString = connection.getMetaData().getIdentifierQuoteString();
        if("SQLite".equals(connection.getMetaData().getDatabaseProductName()))
        {
            createTable();
        }
    }
    
    private void createTable() throws SQLException
    {
        String columns="id AUTONUMBER PRIMARY KEY, firstName TEXT, middleName TEXT, lastName TEXT, dateOfBirth NUMBERIC, married INTEGER";
        
        String sql=String.format("CREATE TABLE IF NOT EXISTS %s (%s)",TABLE_NAME,columns);
        try(Statement stmt = connection.createStatement())
        {
            stmt.execute(sql);
        }
        
    }

    @Override
    public void close() throws Exception
    {
        if(!(connection == null || connection.isClosed()))
        {
            connection.close();
        }
    }
    
    public void insert(Person person) throws SQLException
    {
        String insertSql = String.format("INSERT INTO %s (%s) VALUES(?,?,?,?,?)",getFullTableName(),getInsertColumnNames());
        
        try(PreparedStatement stmt = connection.prepareStatement(insertSql))
        {
            setInsertStatementValues(stmt, person);
            stmt.executeUpdate();
            try(ResultSet genKeys = stmt.getGeneratedKeys())
            {
                if(genKeys.next())
                {
                    person.setId(genKeys.getInt(1));
                }
                
            }
        }
    }

    protected void setInsertStatementValues(final PreparedStatement stmt, Person person) throws SQLException
    {
        stmt.setString(1, person.getFirstName());
        stmt.setString(2, person.getMiddleName());
        stmt.setString(3,person.getLastName());
        
        long dob= person.getDateOfBirth() == null? 0 : person.getDateOfBirth().getTime();
        stmt.setLong(4, dob);
        
        int married = person.isMarried()? 1  : 0;
        stmt.setInt(5, married);
    }
    
    public List<Person> query() throws SQLException
    {
        List<Person> list= new ArrayList<>();
        
        String columns=getQuotedColumnNames();
        String sqlSelect=String.format("SELECT %s FROM %s",columns,getFullTableName());
        try(Statement stmt = connection.createStatement();
                ResultSet rst= stmt.executeQuery(sqlSelect))
        {
            while(rst.next())
            {
                Person p = createPersonFromResultSet(rst);
                
                list.add(p);
                
            }
        }
        
        return list;
    }

    private String getQuotedColumnNames()
    {
        String columns=quoteIdentifiers("id","firstName","middleName","lastName","dateOfBirth","married");
        return columns;
    }
    
    private String getInsertColumnNames()
    {
        String columns=quoteIdentifiers("firstName","middleName","lastName","dateOfBirth","married");
        return columns;
    }

    protected Person createPersonFromResultSet(final ResultSet rst) throws SQLException
    {
        Person p = new Person();
        p.setId(rst.getInt(1));
        p.setFirstName(rst.getString(2));
        p.setMiddleName(rst.getString(3));
        p.setLastName(rst.getString(4));
        Date dob= new Date(rst.getLong(5));
        p.setDateOfBirth(dob);
        boolean married = rst.getInt(6) == 1;
        p.setMarried(married);
        return p;
    }
    
    protected String quoteIdentifier(String identifier)
    {
        return identiferQuoteString + identifier + identiferQuoteString;
    }
    
    protected String quoteIdentifiers(String ... identifiers)
    {
        StringBuilder builder = new StringBuilder();
        for(String ident : identifiers)
        {
            String quoted=quoteIdentifier(ident);
            if(builder.length() > 0)
            {
                builder.append(",");
            }
            builder.append(quoted);
        }
        
        return builder.toString();
    }
    
    public String getFullTableName()
    {
        String name = quoteIdentifier(TABLE_NAME);
        if(!(schema == null || schema.isEmpty()))
        {
            name = quoteIdentifier(schema) + "." + name;
        }
        
        return name;
    }
}
