/*
 * Copyright 2017 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.sandbox;

import com.baseprogramming.dev.gen.DataFactory;
import com.iamcodepoet.sandbox.db.ConnectionFactory;
import com.iamcodepoet.sandbox.db.SqLiteConnectionFactory;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class PersonDaoTest
{
    
    public PersonDaoTest()
    {
        
    }
    
    @Test public void testQuery()
    {
        ConnectionFactory factory= new SqLiteConnectionFactory("people.db");
        try(Connection conn= factory.getConnection();
                PersonDao dao= new PersonDao(conn))
        {
            dao.query().forEach(e->
            {
                System.out.printf("[%s] %s, %s Married: %s\n",e.getId(),e.getLastName(),e.getFirstName(),e.isMarried());
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            assert 0 > 1;
        }
        
        assert 0 > 1;
    }
    
    @Test public void testInsert()
    {
        ConnectionFactory factory= new SqLiteConnectionFactory("people.db");
        try(Connection conn= factory.getConnection();
                PersonDao dao= new PersonDao(conn))
        {
            for(Person p : generatePeople(10))
            {
                dao.insert(p);
                System.out.printf("[%s] %s, %s\n",p.getId(),p.getLastName(),p.getFirstName());
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
            assert 0 > 1;
        }
        
        assert 0 > 1;
    }
    
    public static List<Person> generatePeople(int count)
    {
        List<Person> list= new ArrayList<>();
        
        for(int i=0;i<count;i++)
        {
            Person person=generatePerson();
            
            list.add(person);
        }
        
        return list;
    }

    public static Person generatePerson()
    {
        String first=DataFactory.getFirstNameAnyGender().getWord();
        String middle = DataFactory.getFirstNameAnyGender().getWord();
        String last= DataFactory.getLastName().getWord();
        Date dob= DataFactory.genDate(1912, 2016);
        boolean married=Math.round(100) >= 70;
        Person person = new Person();
        person.setFirstName(first);
        person.setMiddleName(middle);
        person.setLastName(last);
        person.setDateOfBirth(dob);
        person.setMarried(married);
        return person;
    }
    
    
    static
    {
        DataFactory.loadAllCensusNames();
    }
}
