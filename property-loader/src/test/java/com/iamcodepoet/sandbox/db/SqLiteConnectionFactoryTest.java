/*
 * Copyright 2017 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.sandbox.db;

import java.sql.Connection;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class SqLiteConnectionFactoryTest
{
    
    public SqLiteConnectionFactoryTest()
    {
    }
    
    @Test public void testConnection()
    {
        String dbName= "people.db";
        ConnectionFactory factory= new SqLiteConnectionFactory(dbName);
        
        try(Connection conn = factory.getConnection())
        {
            String product = conn.getMetaData().getDatabaseProductName();
            System.out.printf("Product Name: %s\n",product);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        assert 0 > 1;
    }
    
}
