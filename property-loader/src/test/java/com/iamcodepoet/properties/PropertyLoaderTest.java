/* 
 * Copyright 2017 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.properties;

import com.iamcodepoet.sandbox.DaoFactory;
import com.iamcodepoet.sandbox.Person;
import com.iamcodepoet.sandbox.PersonDao;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class PropertyLoaderTest
{

    private String listName;
    private final List<Person> peeps = new ArrayList<>();

    private Person boss;

    private double doubleValue;
    private char character;
    private long longValue;
    private java.sql.Date sqlDate;

    private String variablePropertyValueTest;

    public PropertyLoaderTest()
    {
    }

    @Test
    public void testRegEx()
    {
        String regex = "^\\$\\{[a-zA-z_]\\w*(\\.[a-zA-z_]\\w*)*\\}$";

        String[] values =
        {
            "name", "${firstName}", "${age}", "age", "${456}", "${dao.defaultSchema}"
        };

        for (String value : values)
        {
            boolean matches = value.matches(regex);
            System.out.printf("%s. Is Var: %s\n", value, matches);
        }
        assert 0 > 1;
    }

    @Test
    public void testSomeMethod()
    {

        try (InputStream is = new FileInputStream("dao-config.properties"))
        {
            OrderedProperties props = new OrderedProperties();
            props.load(is);

            DaoFactory factory = DaoFactory.getInstance();
            PropertyLoader loader = new PropertyLoader(factory, "dao");
            loader.load(props);

            try (PersonDao dao = factory.createPersonDao())
            {

                int count = dao.query().size();
                Assert.assertEquals("Invalid Query result", 3, count);

                try
                {
                    System.out.println("\n\nSECOND TEST");

                    PropertyLoader loader2 = new PropertyLoader(this, "junit");
                    loader2.load(props);

                    System.out.println("List Name: " + listName);
                    peeps.forEach(e ->
                    {
                        System.out.printf("%s %s %s (DOB: %s). Married: %s\n", e.getFirstName(), e.getMiddleName(), e.getLastName(), e.getDateOfBirth(), e.isMarried());
                    });

                    if (boss == null)
                    {
                        System.out.println("Boss is null");
                    }
                    else
                    {
                        System.out.printf("BOSS: %s %s %s (%s)\n", boss.getFirstName(), boss.getMiddleName(), boss.getLastName(), boss.getDateOfBirth());
                    }

                    System.out.println("OTHER VALUES:");
                    System.out.println(this);

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();

            }

        }
        catch (IOException e)
        {
            //ignore
        }

    }

    public void addPerson(Person person)
    {
        peeps.add(person);
    }

    public String getListName()
    {
        return listName;
    }

    public void setListName(String listName)
    {
        this.listName = listName;
    }

    public Person getBoss()
    {
        return boss;
    }

    public void setBoss(Person boss)
    {
        this.boss = boss;
    }

    public double getDoubleValue()
    {
        return doubleValue;
    }

    public void setDoubleValue(double doubleValue)
    {
        this.doubleValue = doubleValue;
    }

    public char getCharacter()
    {
        return character;
    }

    public void setCharacter(char character)
    {
        this.character = character;
    }

    public long getLongValue()
    {
        return longValue;
    }

    public void setLongValue(long longValue)
    {
        this.longValue = longValue;
    }

    public Date getSqlDate()
    {
        return sqlDate;
    }

    public void setSqlDate(Date sqlDate)
    {
        this.sqlDate = sqlDate;
    }

    @Override
    public String toString()
    {
        return "PropertyLoaderTest{" + "doubleValue=" + doubleValue + ", character=" + character + ", longValue=" + longValue + ", sqlDate=" + sqlDate + ", variablePropertyValueTest=\"" + variablePropertyValueTest + "\"}";
    }

    public String getVariablePropertyValueTest()
    {
        return variablePropertyValueTest;
    }

    public void setVariablePropertyValueTest(String variablePropertyValueTest)
    {
        this.variablePropertyValueTest = variablePropertyValueTest;
    }

}
